// move router

const routes = {
  home: "/",
  profile: "/profile",
  favorites: "/favorites",
  myArticles: "/my-articles",

  login: "/login",
  register: "/register",

  homeManager: "/manager/home",
  userAdmin: "/manager/admin/student",
  articlesAdmin: "/manager/admin/course",
};

export default routes;
